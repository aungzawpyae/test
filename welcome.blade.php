<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>
    
        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

        <link rel="stylesheet" href="{{ mix('css/app.css') }}">
        <!---- Custom Css --->
        <link href="{{ asset('css/custom.css') }}" rel="stylesheet" type="text/css" >

        <!---- Custom Js --->

        <script src="{{ asset('js/custom.js') }}"></script>

    </head>
    <body class="antialiased scroll-smooth " >
  <!-- Header-->
 
<nav class="fixed w-full z-50 nav-bg top-0">
        <div class="px-2 mx-auto max-w-8xl sm:px-6 lg:px-8">
            <div class="relative flex items-center justify-between h-16">
                <div class="absolute inset-y-0 left-0 flex items-center sm:hidden">
                    <!-- Mobile menu button-->
                    <button type="button" class="inline-flex items-center justify-center p-2 text-gray-400 rounded-md hover:text-white hover:bg-gray-700 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white" aria-controls="mobile-menu" aria-expanded="false">
                    <span class="sr-only">Open main menu</span>
                    <!--
                        Icon when menu is closed.

                        Heroicon name: outline/menu

                        Menu open: "hidden", Menu closed: "block"
                    -->
                    <svg class="block w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 6h16M4 12h16M4 18h16" />
                    </svg>
                    <!--
                        Icon when menu is open.

                        Heroicon name: outline/x

                        Menu open: "block", Menu closed: "hidden"
                    -->
                    <svg class="hidden w-6 h-6" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24" stroke="currentColor" aria-hidden="true">
                        <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M6 18L18 6M6 6l12 12" />
                    </svg>
                    </button>
                </div>
                <div class="flex items-center justify-center flex-1 sm:items-stretch sm:justify-start">
                    <div class="flex items-center flex-shrink-0">
                        <img src="{{ asset('images/logo/Logo.svg')}}" alt="FCS-Logo">
                    </div>
                    
                    <div class="hidden sm:block sm:ml-6 desktop-menu">
                        
                        <div class="flex mt-1 space-x-4 nav__menu " id="nav_menu">
                            <!-- Current: "bg-gray-900 text-white", Default: "text-gray-300 hover:bg-gray-700 hover:text-white" -->
                            <a href="#home" class="link px-3 active py-2 text-lg font-bold text-white" aria-current="page">HOME</a>
                            <a href="#about" class="link  px-3 py-2 text-lg font-bold text-white">ABOUT</a>
                            <a href="#service" class="link  px-3 py-2 text-lg font-bold text-white">SERVICE</a>
                            <a href="#pricing" class="ink px-3 py-2 text-lg font-bold text-white">PRICING</a>
                            <a href="#contact" class=" link px-3 py-2 text-lg font-bold text-white">CONTACT US</a>
                        </div>
                    </div>
                </div>
                <div class="flex justify-end mx-3">
                    <img src="{{ asset('images/logo/twemoji_flag-for-flag-myanmar-burma.png') }}" alt="Myanamr">
                    <h2 class="text-white pl-3"> မြန်မာ</h2>
                </div>
            </div>
        </div>
         <!-- Mobile menu, show/hide based on menu state. -->
        <div class="sm:hidden" id="mobile-menu">
            <div class="px-2 pt-2 pb-3 space-y-1">
            
            <a href="#" class="block px-3 py-2 text-base font-bold text-white bg-gray-900 rounded-md" aria-current="page">Dashboard</a>

            <a href="#" class="block px-3 py-2 text-base font-bold text-gray-300 rounded-md hover:bg-gray-700 hover:text-white">Team</a>

            <a href="#" class="block px-3 py-2 text-base font-bold text-gray-300 rounded-md hover:bg-gray-700 hover:text-white">Projects</a>

            <a href="#" class="block px-3 py-2 text-base font-bold text-gray-300 rounded-md hover:bg-gray-700 hover:text-white">Calendar</a>
            </div>
        </div>
</nav> 

 

  <!-- End Header-->

  <!-- Home section-->
  <section  id="home" class="home md:mb-10  md:mt-20 mt-10">
        <div class="grid grid-cols-3 gap-2">
        
            <div class=" col-span-2  banner-1" >
               <div class="grid grid-cols-2 gap-2">
                   <div class="col  mx-auto flex">
                       <img src="{{ asset('images/home/Loading Your Parcel.svg') }}" alt="">

                       <div class="p-5 hover md:mt-24  rounded-tl-2xl rounded-br-2xl right-8 ">
                           <h1 class="text-white font-bold text-2xl  ">
                               ထိရောက်မြန်ဆန်တဲ့ဝန်ဆောင်မှု
                           </h1>
                       </div>
                       
                   </div>
               </div>
            </div>

            <div class="row-span-3 banner-2 overflow-hidden">
                <div class="relative">
                    <img src="{{ asset('images/home/image-4.png') }}" alt="" class="hover:-translate-x-8   ">
                    
                </div>
            </div>
            
            <div class="">
                <div class="image">
                    <img src="{{ asset('images/home/image-5.png') }}" alt="">
                        <div class="details">
                            <h2>
                                တိချသေချတဲ့ဝန်ဆောင်မှု
                            </h2>
                        </div>
                    </div>
                </div>
            
            <div class="">
                <div class="image">
                    <img src="{{ asset('images/home/image-6.png') }}" alt="">
                    <div class="details">
                        <h2 >
                            ကောင်းမွန်သောစီမံခန်ခွဲမှူ</h2>                 
                    </div>
                </div>
            </div>
      
      </div>
  </section>
  <!-- End Home --->


  <!--- Track Search Bar --->
    {{-- <div class="track-bar">
        <div class="max-w-5xl mx-auto my-8">
            <div class="flex flex-row ">
                <div class="mx-5 text-lg font-bold">
                    Enter Your Tracking Code Numer 
                </div>
                <div>
                    <div class="flex items-center justify-center text-3xl ">
                        <div class="flex border-2 border-gray-200 rounded">
                            <input type="text" class="px-4 py-2 w-80" placeholder="Example : GM12345456YGN">
                            <button class="px-4 text-white bg-gray-900 border-l ">
                                Track
                            </button>
                        </div>
                    </div>
                
            </div>
            
        </div>
    </div> --}}
  <!--- End Track Search Bar --->


  <!--- About Section--->
    <section id="about" class="about " style="background: linear-gradient(308.63deg, #FFE7A0 0.11%, rgba(255, 231, 160, 0.25) 100%);">
        <div class="max-w-md mx-auto text-center">
            <h1 class="pt-8 pb-2 text-3xl font-bold leading-8 tracking-wider">
                ABOUT US
            </h1>
            <h4 class="my-5 text-2xl font-normal leading-7 tracking-tighter">
                FIRST COURIENT SERVICE CO.LTD
            </h4>
        </div>
        <div class="mx-auto max-w-7xl">
            <div class="container flex flex-row">
                        <div class="basis-1/2">
                            <div class="my-5">
                                <h3 class="my-5 text-2xl font-bold leading-7">JANUARY 2015</h3>
                                <h6 class="text-lg italic font-normal">Registered and Established in Yangon, Myanmar.</h6>
                            </div>
                            <div class="my-28">
                                <h3 class="my-5 text-2xl font-bold leading-7">MARCH 2018</h3>
                                <h6 class="text-lg italic font-normal">Obtain Local Delovery Express Service Aualification by Ministry of
                                    Transport and Communications.</h6>
                            </div>
                    
                        </div>
                        <div class="basis-1/2">
                            <div class="relative z-0 overflow-hidden image-hover">
                                <img src="{{ asset('images/about/image 8.svg') }}" alt="" class="hover1">
                                <img src="{{ asset('images/about/image 9.svg') }}" alt="" class="absolute top-0 right-0 overflow-hidden transition duration-300 ease-in-out hover2 hover:-rotate-16 hover:scale-125">
                            </div>
                        </div>
                    </div>
        </div>

    </section>
    <section id="service" class="service ">
        <div class="max-w-5xl mx-auto text-center">
                <h1 class="pt-8 pb-2 text-3xl font-bold leading-8 tracking-wider">
                    SERCICE
                </h1>
                <h4 class="my-5 text-2xl font-normal leading-7 tracking-tighter">
                    About The Service
                </h4>
                <p class="leading-8">
                    Fast Courier Service provides hight-quality door-to-door express delivery services form shipping, distribution to delivery with high efficiency, and promises time-definite delivery service guarantee.
                </p>

                <div class="grid grid-cols-1 md:grid-cols-2 lg:grid-cols-3  gap-10    pt-28">
                    <div class="block overflow-hidden  rounded-lg   hover:shadow-xl bg-white">
                        <div class="overflow-hidden">
                            <img src="{{ asset('images/service/service-1.png') }}" 
                                     class=" w-full  transition duration-700 ease-in-out  hover:scale-125 overflow-hidden"
                                     alt="">
                        </div>
                        <h1 class="p-8 hover:text-orange-400">Domestic Standard Express (2-3 days)</h1>
                    </div>
                    <div class="block overflow-hidden  rounded-lg   hover:shadow-xl bg-white">
                        <div class="overflow-hidden">
                            <img src="{{ asset('images/service/service-2.png') }}"
                                class=" w-full  transition duration-700 ease-in-out  hover:scale-125 overflow-hidden" alt="">
                        </div>
                        <h1 class="p-8 hover:text-orange-400">Domestic Standard Express (2-3 days)</h1>
                    </div>
                    <div class="block overflow-hidden  rounded-lg   hover:shadow-xl bg-white">
                        <div class="overflow-hidden">
                            <img src="{{ asset('images/service/service-3.png') }}"
                                class=" w-full  transition duration-700 ease-in-out  hover:scale-125 overflow-hidden" alt="">
                        </div>
                        <h1 class="p-8 hover:text-orange-400">City Express (Same day or Next day)</h1>
                    </div>
                    <div class="block overflow-hidden  rounded-lg   hover:shadow-xl bg-white">
                        <div class="overflow-hidden">
                            <img src="{{ asset('images/service/servie-4.png')}}"
                                class=" w-full  transition duration-700 ease-in-out  hover:scale-125 overflow-hidden" alt="">
                        </div>
                        <h1 class="p-8 hover:text-orange-400">Online Shop Service</h1>
                    </div>
                    <div class="block overflow-hidden  rounded-lg   hover:shadow-xl bg-white">
                        <div class="overflow-hidden">
                            <img src="{{ asset('images/service/service-5.png') }}"
                                class=" w-full  transition duration-700 ease-in-out  hover:scale-125 overflow-hidden" alt="">
                        </div>
                        <h1 class="p-8 hover:text-orange-400">Money International (4-5 Business days)</h1>
                    </div>
                    <div class="block overflow-hidden  rounded-lg   hover:shadow-xl bg-white">
                        <div class="overflow-hidden">
                            <img src="{{ asset('images/service/service-6.png') }}"
                                class=" w-full  transition duration-700 ease-in-out  hover:scale-125 overflow-hidden" alt="">
                        </div>
                        <h1 class="p-8 hover:text-orange-400">... and more !</h1>
                    </div>
                    
                   
            </div>
                
        </div>
    </section>
        
    

  <!---End About --->

    <!---Service Section--->
    
    <!---End Servie --->


    <!-- Pricing Section--->

    <section id="pricing" class="pricing ">
        <div  class=" overflow-auto container mx-auto">
                <h1 class="pt-8 pb-2 text-3xl font-bold leading-8 tracking-wider text-center">
                    PRICING
                </h1>
                <h4 class="my-5 text-2xl font-normal leading-7 tracking-tighter text-center">
                    DOMESTIC
                </h4>
                <div class="table">
                    <div class="flex flex-col">
                        <div class="overflow-x-auto shadow-md sm:rounded-lg">
                            <div class="inline-block min-w-full align-middle">
                                <div class="overflow-hidden ">
                                    <table class="min-w-full divide-y divide-gray-200 table-fixed dark:divide-gray-700">
                                        <thead class="bg-gray-100 dark:bg-gray-700">
                                            <tr class="border">
                                                <th scope="col" colspan="2"
                                                    class=" text-center py-3 font-bold px-6 text-xs  tracking-wider  text-gray-700 uppercase dark:text-gray-400">
                                                    Item/Weight
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    YGN(1)
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    MDY (1+2)
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    MDY (3)
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    NPT(1)
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    NPT(2)
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    TGI
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    TGI(ATY)
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    LSO
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    MOG
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    MNK
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    MLM
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    POL
                                                </th>
                                                <th scope="col"
                                                    class="py-3 px-6 text-xs font-bold tracking-wider text-left text-gray-700 uppercase dark:text-gray-400">
                                                    SIG
                                                </th>
                                            </tr>
                                        </thead>
                                        <tbody class="  bg-white divide-y divide-gray-200 dark:bg-gray-800 dark:divide-gray-700">
                                            <tr class="hover:bg-gray-100 dark:hover:bg-gray-700">
                                                <td
                                                    class="border  py-4 px-6 text-sm font-bold  text-gray-900 whitespace-nowrap dark:text-white">
                                                    <h5 class="pb-3">YGN</h5>
                                                    <H5>Yangon</H5>
                                                </td>
                                                <td
                                                    class=" border  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6>1 Kg</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td
                                                    class="  py-4 border-r px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
            
            
                                            </tr>
                                            <tr class="hover:bg-gray-100 dark:hover:bg-gray-700">
                                                <td
                                                    class="border  py-4 px-6 text-sm font-bold  text-gray-900 whitespace-nowrap dark:text-white">
                                                    <h5 class="pb-3 uppercase">mdy</h5>
                                                    <H5>Mandalay</H5>
                                                </td>
                                                <td
                                                    class=" border  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6>1 Kg</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3"></h6>
                                                    <h6></h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td
                                                    class="border-r  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
            
                                            </tr>
                                            <tr class="hover:bg-gray-100 dark:hover:bg-gray-700">
                                                <td
                                                    class="border  py-4 px-6 text-sm font-bold  text-gray-900 whitespace-nowrap dark:text-white">
                                                    <h5 class="pb-3 uppercase">NPT</h5>
                                                    <H5>Naypyitaw</H5>
                                                </td>
                                                <td
                                                    class=" border  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6>1 Kg</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3"></h6>
                                                    <h6></h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td
                                                    class=" border-r  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
            
                                            </tr>
                                            <tr class="hover:bg-gray-100 dark:hover:bg-gray-700">
                                                <td
                                                    class="border  py-4 px-6 text-sm font-bold  text-gray-900 whitespace-nowrap dark:text-white">
                                                    <h5 class="pb-3 uppercase">tgi</h5>
                                                    <H5>Taungyi</H5>
                                                </td>
                                                <td
                                                    class=" border  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6>1 Kg</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td
                                                    class=" border-r  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
            
                                            </tr>
                                            <tr class="hover:bg-gray-100 dark:hover:bg-gray-700">
                                                <td
                                                    class="border  py-4 px-6 text-sm font-bold  text-gray-900 whitespace-nowrap dark:text-white">
                                                    <h5 class="pb-3 uppercase">lso</h5>
                                                    <H5>Lashio</H5>
                                                </td>
                                                <td
                                                    class=" border py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6>1 Kg</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td class="  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                                <td
                                                    class=" border-r  py-4 px-6 text-sm  text-gray-500 whitespace-nowrap dark:text-white">
                                                    <h6 class="mb-3">1,500</h6>
                                                    <h6>(+500)</h6>
                                                </td>
                                            </tr>
            
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
        </div>
    </section>
    <!--- End Pricing --->

    <section class="container mx-auto my-8">
        <div class="border-l-4 border-l-green-500 ml-3  pb-16">
            <div class="relative">
                <div class="absolute top-0 -left-3 ">
                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path fill-rule="evenodd" clip-rule="evenodd"
                            d="M13 0.166672C5.91246 0.166672 0.166626 5.91251 0.166626 13C0.166626 20.0875 5.91246 25.8333 13 25.8333C20.0875 25.8333 25.8333 20.0875 25.8333 13C25.8333 5.91251 20.0875 0.166672 13 0.166672ZM18.5626 10.83C18.6651 10.7129 18.743 10.5765 18.792 10.4289C18.8409 10.2812 18.8598 10.1252 18.8476 9.97015C18.8354 9.81507 18.7923 9.664 18.7208 9.52582C18.6493 9.38765 18.5509 9.26516 18.4314 9.16557C18.3119 9.06598 18.1737 8.99129 18.0249 8.94591C17.8761 8.90053 17.7197 8.88536 17.565 8.9013C17.4102 8.91725 17.2602 8.96398 17.1238 9.03874C16.9874 9.11351 16.8673 9.21481 16.7706 9.33667L11.754 15.3555L9.15813 12.7585C8.93809 12.546 8.64339 12.4284 8.33749 12.4311C8.0316 12.4337 7.73898 12.5564 7.52267 12.7727C7.30636 12.989 7.18366 13.2816 7.18101 13.5875C7.17835 13.8934 7.29594 14.1881 7.50846 14.4082L11.0085 17.9082C11.1231 18.0227 11.2603 18.1122 11.4114 18.1707C11.5626 18.2293 11.7242 18.2558 11.8861 18.2484C12.048 18.2411 12.2066 18.2001 12.3518 18.128C12.4969 18.056 12.6255 17.9545 12.7293 17.83L18.5626 10.83Z"
                            fill="#E5463B" />
                    </svg>
                </div>
                    <div class="pl-10">
                        <div class="grid grid-flow-col auto-cols-max">
                            <div class="flex">
                                <svg class="mt-3" width="64" height="4" viewBox="0 0 68 4" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M2 2L66 2" stroke="#D3D3D3" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"
                                        stroke-dasharray="6 8" />
                                </svg>
                                <svg class="-mt-2" width="38" height="38" viewBox="0 0 38 38" fill="none" xmlns="http://www.w3.org/2000/svg">
                                    <g clip-path="url(#clip0_1161_1561)">
                                        <path d="M31.3496 15.6748H6.6499V21.3748H31.3496V15.6748Z" fill="#F5F7FA" />
                                        <path
                                            d="M31.3496 32.774V16.6241C31.3496 16.1004 30.9234 15.6742 30.3997 15.6742H7.59992C7.07617 15.6742 6.64993 16.1004 6.64993 16.6241V32.774H2.84998V12.0243L18.9998 5.22495L35.1496 12.0243V32.774L31.3496 32.774Z"
                                            fill="#FFCE54" />
                                        <path d="M31.3496 21.3748H6.6499V27.0747H31.3496V21.3748Z" fill="#F5F7FA" />
                                        <path d="M31.3496 27.0747H6.6499V32.7746H31.3496V27.0747Z" fill="#656D78" />
                                        <path
                                            d="M1.31855 13.7007L1.89998 13.4556V31.824H0.949989C0.424972 31.824 0 32.2489 0 32.7739C0 33.299 0.424972 33.7239 0.949989 33.7239H37.0496C37.5746 33.7239 37.9996 33.299 37.9996 32.7739C37.9996 32.2489 37.5746 31.824 37.0496 31.824H36.0996V13.4556L36.681 13.7007C36.8007 13.7507 36.9261 13.7748 37.0496 13.7748C37.4194 13.7748 37.7722 13.5569 37.9255 13.1933C38.1288 12.7101 37.902 12.1534 37.4182 11.9495L19.3684 4.34961C19.1329 4.25142 18.8674 4.25142 18.6319 4.34961L0.582091 11.9495C0.0975965 12.1535 -0.128471 12.7102 0.0748116 13.1933C0.278688 13.6779 0.839182 13.9009 1.31855 13.7007ZM30.3996 31.824H7.59991V28.024H30.3996V31.824ZM30.3996 26.124H7.59991V22.3241H30.3996V26.124ZM30.3996 20.4248H7.59991V16.6248H30.3996V20.4248ZM3.79996 12.6557L18.9998 6.25538L34.1996 12.6551V31.824H32.2996V16.6241C32.2996 15.5747 31.4491 14.7242 30.3996 14.7242H7.59991C6.55047 14.7242 5.69993 15.5747 5.69993 16.6241V31.824H3.79996V12.6557Z"
                                            fill="#F6BB42" />
                                        <path d="M19 6.04546L3.88635 12.5227V13.8182L19 7.77273L34.1136 13.9942V12.5227L19 6.04546Z" fill="#E2A63B" />
                                        <path
                                            d="M35.1172 3.6535C34.7507 3.34817 34.2061 3.39767 33.9006 3.76412C33.9002 3.76466 33.8997 3.76525 33.8992 3.76579L33.2687 4.52589V1.72739C33.2687 1.25032 32.882 0.863632 32.4049 0.863632C31.9279 0.863632 31.5412 1.25032 31.5412 1.72739V4.52589L30.9106 3.76579C30.6187 3.38844 30.0763 3.31923 29.6989 3.61107C29.3216 3.90297 29.2524 4.44541 29.5442 4.82276C29.5571 4.83944 29.5706 4.85563 29.5848 4.87134L31.7442 7.46261C32.05 7.82873 32.5947 7.87764 32.9608 7.57182C33.0004 7.53878 33.037 7.50218 33.07 7.46261L35.2294 4.87134C35.5347 4.50484 35.4852 3.96024 35.1188 3.6548C35.1183 3.65442 35.1177 3.65399 35.1172 3.6535Z"
                                            fill="#FFCE54" />
                                        <path
                                            d="M35.1171 3.6535C35.0566 3.60443 34.9898 3.56378 34.9184 3.53258C35.0547 3.83316 35.0078 4.18514 34.7975 4.43952L32.6381 7.03078C32.3663 7.35129 31.9033 7.42973 31.5411 7.21649L31.7441 7.46266C32.0499 7.82878 32.5946 7.87769 32.9608 7.57187C33.0004 7.53883 33.0369 7.50223 33.07 7.46266L35.2294 4.8714C35.5347 4.5049 35.4852 3.9603 35.1187 3.65485C35.1182 3.65442 35.1176 3.65399 35.1171 3.6535Z"
                                            fill="#E2A63B" />
                                        <path
                                            d="M33.2688 4.5259C33.0303 4.5259 32.8369 4.33253 32.8369 4.09402V1.29551C32.8364 1.17329 32.8098 1.05258 32.7592 0.941376C33.0687 1.08049 33.2681 1.38799 33.2688 1.72739V3.12665V4.5259Z"
                                            fill="#E2A63B" />
                                    </g>
                                    <defs>
                                        <clipPath id="clip0_1161_1561">
                                            <rect width="38" height="38" fill="white" />
                                        </clipPath>
                                    </defs>
                                </svg>
                            </div>
                            <div class=" ml-5">
                                <h3 class=""> 
                                    လိပ်စာရှင်ထံသို့ရောက်ရှိ
                                </h3>
                                <p class="text-yellow-800">
                                    <span class="text-red-600">အခြေအနေ</span>
                                    လိပ်စာရှင်ပြောင်းရွေ့သွားပါသည်
                                </p>
                            </div>
                            

                        </div>
                    </div>
            </div>
        </div>

        <div class="border-l-4  border-l-green-500 ml-3  pb-16">
                    <div class="relative">
                        <div class="absolute top-0 -left-3">
                            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M13 0.166672C5.91246 0.166672 0.166626 5.91251 0.166626 13C0.166626 20.0875 5.91246 25.8333 13 25.8333C20.0875 25.8333 25.8333 20.0875 25.8333 13C25.8333 5.91251 20.0875 0.166672 13 0.166672ZM18.5626 10.83C18.6651 10.7129 18.743 10.5765 18.792 10.4289C18.8409 10.2812 18.8598 10.1252 18.8476 9.97015C18.8354 9.81507 18.7923 9.664 18.7208 9.52582C18.6493 9.38765 18.5509 9.26516 18.4314 9.16557C18.3119 9.06598 18.1737 8.99129 18.0249 8.94591C17.8761 8.90053 17.7197 8.88536 17.565 8.9013C17.4102 8.91725 17.2602 8.96398 17.1238 9.03874C16.9874 9.11351 16.8673 9.21481 16.7706 9.33667L11.754 15.3555L9.15813 12.7585C8.93809 12.546 8.64339 12.4284 8.33749 12.4311C8.0316 12.4337 7.73898 12.5564 7.52267 12.7727C7.30636 12.989 7.18366 13.2816 7.18101 13.5875C7.17835 13.8934 7.29594 14.1881 7.50846 14.4082L11.0085 17.9082C11.1231 18.0227 11.2603 18.1122 11.4114 18.1707C11.5626 18.2293 11.7242 18.2558 11.8861 18.2484C12.048 18.2411 12.2066 18.2001 12.3518 18.128C12.4969 18.056 12.6255 17.9545 12.7293 17.83L18.5626 10.83Z"
                                    fill="#E5463B" />
                            </svg>
                        </div>
                        <div class="pl-10">
                            <div class="grid grid-flow-col auto-cols-max">
                                <div class="flex">
                                    <svg class="mt-3" width="64" height="4" viewBox="0 0 68 4" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M2 2L66 2" stroke="#D3D3D3" stroke-width="4" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-dasharray="6 8" />
                                    </svg>
                                    <svg class="-mt-2" width="38" height="38" viewBox="0 0 38 38" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0_1161_1561)">
                                            <path d="M31.3496 15.6748H6.6499V21.3748H31.3496V15.6748Z" fill="#F5F7FA" />
                                            <path
                                                d="M31.3496 32.774V16.6241C31.3496 16.1004 30.9234 15.6742 30.3997 15.6742H7.59992C7.07617 15.6742 6.64993 16.1004 6.64993 16.6241V32.774H2.84998V12.0243L18.9998 5.22495L35.1496 12.0243V32.774L31.3496 32.774Z"
                                                fill="#FFCE54" />
                                            <path d="M31.3496 21.3748H6.6499V27.0747H31.3496V21.3748Z" fill="#F5F7FA" />
                                            <path d="M31.3496 27.0747H6.6499V32.7746H31.3496V27.0747Z" fill="#656D78" />
                                            <path
                                                d="M1.31855 13.7007L1.89998 13.4556V31.824H0.949989C0.424972 31.824 0 32.2489 0 32.7739C0 33.299 0.424972 33.7239 0.949989 33.7239H37.0496C37.5746 33.7239 37.9996 33.299 37.9996 32.7739C37.9996 32.2489 37.5746 31.824 37.0496 31.824H36.0996V13.4556L36.681 13.7007C36.8007 13.7507 36.9261 13.7748 37.0496 13.7748C37.4194 13.7748 37.7722 13.5569 37.9255 13.1933C38.1288 12.7101 37.902 12.1534 37.4182 11.9495L19.3684 4.34961C19.1329 4.25142 18.8674 4.25142 18.6319 4.34961L0.582091 11.9495C0.0975965 12.1535 -0.128471 12.7102 0.0748116 13.1933C0.278688 13.6779 0.839182 13.9009 1.31855 13.7007ZM30.3996 31.824H7.59991V28.024H30.3996V31.824ZM30.3996 26.124H7.59991V22.3241H30.3996V26.124ZM30.3996 20.4248H7.59991V16.6248H30.3996V20.4248ZM3.79996 12.6557L18.9998 6.25538L34.1996 12.6551V31.824H32.2996V16.6241C32.2996 15.5747 31.4491 14.7242 30.3996 14.7242H7.59991C6.55047 14.7242 5.69993 15.5747 5.69993 16.6241V31.824H3.79996V12.6557Z"
                                                fill="#F6BB42" />
                                            <path d="M19 6.04546L3.88635 12.5227V13.8182L19 7.77273L34.1136 13.9942V12.5227L19 6.04546Z"
                                                fill="#E2A63B" />
                                            <path
                                                d="M35.1172 3.6535C34.7507 3.34817 34.2061 3.39767 33.9006 3.76412C33.9002 3.76466 33.8997 3.76525 33.8992 3.76579L33.2687 4.52589V1.72739C33.2687 1.25032 32.882 0.863632 32.4049 0.863632C31.9279 0.863632 31.5412 1.25032 31.5412 1.72739V4.52589L30.9106 3.76579C30.6187 3.38844 30.0763 3.31923 29.6989 3.61107C29.3216 3.90297 29.2524 4.44541 29.5442 4.82276C29.5571 4.83944 29.5706 4.85563 29.5848 4.87134L31.7442 7.46261C32.05 7.82873 32.5947 7.87764 32.9608 7.57182C33.0004 7.53878 33.037 7.50218 33.07 7.46261L35.2294 4.87134C35.5347 4.50484 35.4852 3.96024 35.1188 3.6548C35.1183 3.65442 35.1177 3.65399 35.1172 3.6535Z"
                                                fill="#FFCE54" />
                                            <path
                                                d="M35.1171 3.6535C35.0566 3.60443 34.9898 3.56378 34.9184 3.53258C35.0547 3.83316 35.0078 4.18514 34.7975 4.43952L32.6381 7.03078C32.3663 7.35129 31.9033 7.42973 31.5411 7.21649L31.7441 7.46266C32.0499 7.82878 32.5946 7.87769 32.9608 7.57187C33.0004 7.53883 33.0369 7.50223 33.07 7.46266L35.2294 4.8714C35.5347 4.5049 35.4852 3.9603 35.1187 3.65485C35.1182 3.65442 35.1176 3.65399 35.1171 3.6535Z"
                                                fill="#E2A63B" />
                                            <path
                                                d="M33.2688 4.5259C33.0303 4.5259 32.8369 4.33253 32.8369 4.09402V1.29551C32.8364 1.17329 32.8098 1.05258 32.7592 0.941376C33.0687 1.08049 33.2681 1.38799 33.2688 1.72739V3.12665V4.5259Z"
                                                fill="#E2A63B" />
                                        </g>
                                        <defs>
                                            <clipPath id="clip0_1161_1561">
                                                <rect width="38" height="38" fill="white" />
                                            </clipPath>
                                        </defs>
                                    </svg>
                                </div>
                             
                
                            </div>
                        </div>
                    </div>
                </div>
                <div class="border-l-4 border-l-green-500 ml-3  pb-16">
                            <div class="relative">
                                <div class="absolute top-0 -left-3">
                                    <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" clip-rule="evenodd"
                                            d="M13 0.166672C5.91246 0.166672 0.166626 5.91251 0.166626 13C0.166626 20.0875 5.91246 25.8333 13 25.8333C20.0875 25.8333 25.8333 20.0875 25.8333 13C25.8333 5.91251 20.0875 0.166672 13 0.166672ZM18.5626 10.83C18.6651 10.7129 18.743 10.5765 18.792 10.4289C18.8409 10.2812 18.8598 10.1252 18.8476 9.97015C18.8354 9.81507 18.7923 9.664 18.7208 9.52582C18.6493 9.38765 18.5509 9.26516 18.4314 9.16557C18.3119 9.06598 18.1737 8.99129 18.0249 8.94591C17.8761 8.90053 17.7197 8.88536 17.565 8.9013C17.4102 8.91725 17.2602 8.96398 17.1238 9.03874C16.9874 9.11351 16.8673 9.21481 16.7706 9.33667L11.754 15.3555L9.15813 12.7585C8.93809 12.546 8.64339 12.4284 8.33749 12.4311C8.0316 12.4337 7.73898 12.5564 7.52267 12.7727C7.30636 12.989 7.18366 13.2816 7.18101 13.5875C7.17835 13.8934 7.29594 14.1881 7.50846 14.4082L11.0085 17.9082C11.1231 18.0227 11.2603 18.1122 11.4114 18.1707C11.5626 18.2293 11.7242 18.2558 11.8861 18.2484C12.048 18.2411 12.2066 18.2001 12.3518 18.128C12.4969 18.056 12.6255 17.9545 12.7293 17.83L18.5626 10.83Z"
                                            fill="#E5463B" />
                                    </svg>
                                </div>
                                <div class="pl-10">
                                    <div class="grid grid-flow-col auto-cols-max">
                                        <div class="flex">
                                            <svg class="mt-3" width="64" height="4" viewBox="0 0 68 4" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <path d="M2 2L66 2" stroke="#D3D3D3" stroke-width="4" stroke-linecap="round"
                                                    stroke-linejoin="round" stroke-dasharray="6 8" />
                                            </svg>
                                            <svg class="-mt-2" width="38" height="38" viewBox="0 0 38 38" fill="none"
                                                xmlns="http://www.w3.org/2000/svg">
                                                <g clip-path="url(#clip0_1161_1561)">
                                                    <path d="M31.3496 15.6748H6.6499V21.3748H31.3496V15.6748Z" fill="#F5F7FA" />
                                                    <path
                                                        d="M31.3496 32.774V16.6241C31.3496 16.1004 30.9234 15.6742 30.3997 15.6742H7.59992C7.07617 15.6742 6.64993 16.1004 6.64993 16.6241V32.774H2.84998V12.0243L18.9998 5.22495L35.1496 12.0243V32.774L31.3496 32.774Z"
                                                        fill="#FFCE54" />
                                                    <path d="M31.3496 21.3748H6.6499V27.0747H31.3496V21.3748Z" fill="#F5F7FA" />
                                                    <path d="M31.3496 27.0747H6.6499V32.7746H31.3496V27.0747Z" fill="#656D78" />
                                                    <path
                                                        d="M1.31855 13.7007L1.89998 13.4556V31.824H0.949989C0.424972 31.824 0 32.2489 0 32.7739C0 33.299 0.424972 33.7239 0.949989 33.7239H37.0496C37.5746 33.7239 37.9996 33.299 37.9996 32.7739C37.9996 32.2489 37.5746 31.824 37.0496 31.824H36.0996V13.4556L36.681 13.7007C36.8007 13.7507 36.9261 13.7748 37.0496 13.7748C37.4194 13.7748 37.7722 13.5569 37.9255 13.1933C38.1288 12.7101 37.902 12.1534 37.4182 11.9495L19.3684 4.34961C19.1329 4.25142 18.8674 4.25142 18.6319 4.34961L0.582091 11.9495C0.0975965 12.1535 -0.128471 12.7102 0.0748116 13.1933C0.278688 13.6779 0.839182 13.9009 1.31855 13.7007ZM30.3996 31.824H7.59991V28.024H30.3996V31.824ZM30.3996 26.124H7.59991V22.3241H30.3996V26.124ZM30.3996 20.4248H7.59991V16.6248H30.3996V20.4248ZM3.79996 12.6557L18.9998 6.25538L34.1996 12.6551V31.824H32.2996V16.6241C32.2996 15.5747 31.4491 14.7242 30.3996 14.7242H7.59991C6.55047 14.7242 5.69993 15.5747 5.69993 16.6241V31.824H3.79996V12.6557Z"
                                                        fill="#F6BB42" />
                                                    <path d="M19 6.04546L3.88635 12.5227V13.8182L19 7.77273L34.1136 13.9942V12.5227L19 6.04546Z"
                                                        fill="#E2A63B" />
                                                    <path
                                                        d="M35.1172 3.6535C34.7507 3.34817 34.2061 3.39767 33.9006 3.76412C33.9002 3.76466 33.8997 3.76525 33.8992 3.76579L33.2687 4.52589V1.72739C33.2687 1.25032 32.882 0.863632 32.4049 0.863632C31.9279 0.863632 31.5412 1.25032 31.5412 1.72739V4.52589L30.9106 3.76579C30.6187 3.38844 30.0763 3.31923 29.6989 3.61107C29.3216 3.90297 29.2524 4.44541 29.5442 4.82276C29.5571 4.83944 29.5706 4.85563 29.5848 4.87134L31.7442 7.46261C32.05 7.82873 32.5947 7.87764 32.9608 7.57182C33.0004 7.53878 33.037 7.50218 33.07 7.46261L35.2294 4.87134C35.5347 4.50484 35.4852 3.96024 35.1188 3.6548C35.1183 3.65442 35.1177 3.65399 35.1172 3.6535Z"
                                                        fill="#FFCE54" />
                                                    <path
                                                        d="M35.1171 3.6535C35.0566 3.60443 34.9898 3.56378 34.9184 3.53258C35.0547 3.83316 35.0078 4.18514 34.7975 4.43952L32.6381 7.03078C32.3663 7.35129 31.9033 7.42973 31.5411 7.21649L31.7441 7.46266C32.0499 7.82878 32.5946 7.87769 32.9608 7.57187C33.0004 7.53883 33.0369 7.50223 33.07 7.46266L35.2294 4.8714C35.5347 4.5049 35.4852 3.9603 35.1187 3.65485C35.1182 3.65442 35.1176 3.65399 35.1171 3.6535Z"
                                                        fill="#E2A63B" />
                                                    <path
                                                        d="M33.2688 4.5259C33.0303 4.5259 32.8369 4.33253 32.8369 4.09402V1.29551C32.8364 1.17329 32.8098 1.05258 32.7592 0.941376C33.0687 1.08049 33.2681 1.38799 33.2688 1.72739V3.12665V4.5259Z"
                                                        fill="#E2A63B" />
                                                </g>
                                                <defs>
                                                    <clipPath id="clip0_1161_1561">
                                                        <rect width="38" height="38" fill="white" />
                                                    </clipPath>
                                                </defs>
                                            </svg>
                                        </div>
                                 
                        
                                    </div>
                                </div>
                            </div>
                        </div>
        <div class="border-l-4 border-l-green-500 ml-3  pb-16">
                    <div class="relative">
                        <div class="absolute top-0 -left-3">
                            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M13 0.166672C5.91246 0.166672 0.166626 5.91251 0.166626 13C0.166626 20.0875 5.91246 25.8333 13 25.8333C20.0875 25.8333 25.8333 20.0875 25.8333 13C25.8333 5.91251 20.0875 0.166672 13 0.166672ZM18.5626 10.83C18.6651 10.7129 18.743 10.5765 18.792 10.4289C18.8409 10.2812 18.8598 10.1252 18.8476 9.97015C18.8354 9.81507 18.7923 9.664 18.7208 9.52582C18.6493 9.38765 18.5509 9.26516 18.4314 9.16557C18.3119 9.06598 18.1737 8.99129 18.0249 8.94591C17.8761 8.90053 17.7197 8.88536 17.565 8.9013C17.4102 8.91725 17.2602 8.96398 17.1238 9.03874C16.9874 9.11351 16.8673 9.21481 16.7706 9.33667L11.754 15.3555L9.15813 12.7585C8.93809 12.546 8.64339 12.4284 8.33749 12.4311C8.0316 12.4337 7.73898 12.5564 7.52267 12.7727C7.30636 12.989 7.18366 13.2816 7.18101 13.5875C7.17835 13.8934 7.29594 14.1881 7.50846 14.4082L11.0085 17.9082C11.1231 18.0227 11.2603 18.1122 11.4114 18.1707C11.5626 18.2293 11.7242 18.2558 11.8861 18.2484C12.048 18.2411 12.2066 18.2001 12.3518 18.128C12.4969 18.056 12.6255 17.9545 12.7293 17.83L18.5626 10.83Z"
                                    fill="#E5463B" />
                            </svg>
                        </div>
                        <div class="pl-10">
                            <div class="grid grid-flow-col auto-cols-max">
                                <div class="flex">
                                    <svg class="mt-3" width="64" height="4" viewBox="0 0 68 4" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M2 2L66 2" stroke="#D3D3D3" stroke-width="4" stroke-linecap="round"
                                            stroke-linejoin="round" stroke-dasharray="6 8" />
                                    </svg>
                                    <svg class="-mt-2" width="38" height="38" viewBox="0 0 38 38" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0_1161_1561)">
                                            <path d="M31.3496 15.6748H6.6499V21.3748H31.3496V15.6748Z" fill="#F5F7FA" />
                                            <path
                                                d="M31.3496 32.774V16.6241C31.3496 16.1004 30.9234 15.6742 30.3997 15.6742H7.59992C7.07617 15.6742 6.64993 16.1004 6.64993 16.6241V32.774H2.84998V12.0243L18.9998 5.22495L35.1496 12.0243V32.774L31.3496 32.774Z"
                                                fill="#FFCE54" />
                                            <path d="M31.3496 21.3748H6.6499V27.0747H31.3496V21.3748Z" fill="#F5F7FA" />
                                            <path d="M31.3496 27.0747H6.6499V32.7746H31.3496V27.0747Z" fill="#656D78" />
                                            <path
                                                d="M1.31855 13.7007L1.89998 13.4556V31.824H0.949989C0.424972 31.824 0 32.2489 0 32.7739C0 33.299 0.424972 33.7239 0.949989 33.7239H37.0496C37.5746 33.7239 37.9996 33.299 37.9996 32.7739C37.9996 32.2489 37.5746 31.824 37.0496 31.824H36.0996V13.4556L36.681 13.7007C36.8007 13.7507 36.9261 13.7748 37.0496 13.7748C37.4194 13.7748 37.7722 13.5569 37.9255 13.1933C38.1288 12.7101 37.902 12.1534 37.4182 11.9495L19.3684 4.34961C19.1329 4.25142 18.8674 4.25142 18.6319 4.34961L0.582091 11.9495C0.0975965 12.1535 -0.128471 12.7102 0.0748116 13.1933C0.278688 13.6779 0.839182 13.9009 1.31855 13.7007ZM30.3996 31.824H7.59991V28.024H30.3996V31.824ZM30.3996 26.124H7.59991V22.3241H30.3996V26.124ZM30.3996 20.4248H7.59991V16.6248H30.3996V20.4248ZM3.79996 12.6557L18.9998 6.25538L34.1996 12.6551V31.824H32.2996V16.6241C32.2996 15.5747 31.4491 14.7242 30.3996 14.7242H7.59991C6.55047 14.7242 5.69993 15.5747 5.69993 16.6241V31.824H3.79996V12.6557Z"
                                                fill="#F6BB42" />
                                            <path d="M19 6.04546L3.88635 12.5227V13.8182L19 7.77273L34.1136 13.9942V12.5227L19 6.04546Z"
                                                fill="#E2A63B" />
                                            <path
                                                d="M35.1172 3.6535C34.7507 3.34817 34.2061 3.39767 33.9006 3.76412C33.9002 3.76466 33.8997 3.76525 33.8992 3.76579L33.2687 4.52589V1.72739C33.2687 1.25032 32.882 0.863632 32.4049 0.863632C31.9279 0.863632 31.5412 1.25032 31.5412 1.72739V4.52589L30.9106 3.76579C30.6187 3.38844 30.0763 3.31923 29.6989 3.61107C29.3216 3.90297 29.2524 4.44541 29.5442 4.82276C29.5571 4.83944 29.5706 4.85563 29.5848 4.87134L31.7442 7.46261C32.05 7.82873 32.5947 7.87764 32.9608 7.57182C33.0004 7.53878 33.037 7.50218 33.07 7.46261L35.2294 4.87134C35.5347 4.50484 35.4852 3.96024 35.1188 3.6548C35.1183 3.65442 35.1177 3.65399 35.1172 3.6535Z"
                                                fill="#FFCE54" />
                                            <path
                                                d="M35.1171 3.6535C35.0566 3.60443 34.9898 3.56378 34.9184 3.53258C35.0547 3.83316 35.0078 4.18514 34.7975 4.43952L32.6381 7.03078C32.3663 7.35129 31.9033 7.42973 31.5411 7.21649L31.7441 7.46266C32.0499 7.82878 32.5946 7.87769 32.9608 7.57187C33.0004 7.53883 33.0369 7.50223 33.07 7.46266L35.2294 4.8714C35.5347 4.5049 35.4852 3.9603 35.1187 3.65485C35.1182 3.65442 35.1176 3.65399 35.1171 3.6535Z"
                                                fill="#E2A63B" />
                                            <path
                                                d="M33.2688 4.5259C33.0303 4.5259 32.8369 4.33253 32.8369 4.09402V1.29551C32.8364 1.17329 32.8098 1.05258 32.7592 0.941376C33.0687 1.08049 33.2681 1.38799 33.2688 1.72739V3.12665V4.5259Z"
                                                fill="#E2A63B" />
                                        </g>
                                        <defs>
                                            <clipPath id="clip0_1161_1561">
                                                <rect width="38" height="38" fill="white" />
                                            </clipPath>
                                        </defs>
                                    </svg>
                                </div>
                             
                
                
                            </div>
                        </div>
                    </div>
                    <div class="relative top-20">
                        <div class="absolute top-0 -left-3">
                            <svg width="26" height="26" viewBox="0 0 26 26" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" clip-rule="evenodd"
                                    d="M13 0.166672C5.91246 0.166672 0.166626 5.91251 0.166626 13C0.166626 20.0875 5.91246 25.8333 13 25.8333C20.0875 25.8333 25.8333 20.0875 25.8333 13C25.8333 5.91251 20.0875 0.166672 13 0.166672ZM18.5626 10.83C18.6651 10.7129 18.743 10.5765 18.792 10.4289C18.8409 10.2812 18.8598 10.1252 18.8476 9.97015C18.8354 9.81507 18.7923 9.664 18.7208 9.52582C18.6493 9.38765 18.5509 9.26516 18.4314 9.16557C18.3119 9.06598 18.1737 8.99129 18.0249 8.94591C17.8761 8.90053 17.7197 8.88536 17.565 8.9013C17.4102 8.91725 17.2602 8.96398 17.1238 9.03874C16.9874 9.11351 16.8673 9.21481 16.7706 9.33667L11.754 15.3555L9.15813 12.7585C8.93809 12.546 8.64339 12.4284 8.33749 12.4311C8.0316 12.4337 7.73898 12.5564 7.52267 12.7727C7.30636 12.989 7.18366 13.2816 7.18101 13.5875C7.17835 13.8934 7.29594 14.1881 7.50846 14.4082L11.0085 17.9082C11.1231 18.0227 11.2603 18.1122 11.4114 18.1707C11.5626 18.2293 11.7242 18.2558 11.8861 18.2484C12.048 18.2411 12.2066 18.2001 12.3518 18.128C12.4969 18.056 12.6255 17.9545 12.7293 17.83L18.5626 10.83Z"
                                    fill="#E5463B" />
                            </svg>
                        </div>
                        <div class="pl-10">
                            <div class="grid grid-flow-col auto-cols-max">
                                <div class="flex">
                                    <svg class="mt-3" width="64" height="4" viewBox="0 0 68 4" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <path d="M2 2L66 2" stroke="#D3D3D3" stroke-width="4" stroke-linecap="round" stroke-linejoin="round"
                                            stroke-dasharray="6 8" />
                                    </svg>
                                    <svg class="-mt-2" width="38" height="38" viewBox="0 0 38 38" fill="none"
                                        xmlns="http://www.w3.org/2000/svg">
                                        <g clip-path="url(#clip0_1161_1561)">
                                            <path d="M31.3496 15.6748H6.6499V21.3748H31.3496V15.6748Z" fill="#F5F7FA" />
                                            <path
                                                d="M31.3496 32.774V16.6241C31.3496 16.1004 30.9234 15.6742 30.3997 15.6742H7.59992C7.07617 15.6742 6.64993 16.1004 6.64993 16.6241V32.774H2.84998V12.0243L18.9998 5.22495L35.1496 12.0243V32.774L31.3496 32.774Z"
                                                fill="#FFCE54" />
                                            <path d="M31.3496 21.3748H6.6499V27.0747H31.3496V21.3748Z" fill="#F5F7FA" />
                                            <path d="M31.3496 27.0747H6.6499V32.7746H31.3496V27.0747Z" fill="#656D78" />
                                            <path
                                                d="M1.31855 13.7007L1.89998 13.4556V31.824H0.949989C0.424972 31.824 0 32.2489 0 32.7739C0 33.299 0.424972 33.7239 0.949989 33.7239H37.0496C37.5746 33.7239 37.9996 33.299 37.9996 32.7739C37.9996 32.2489 37.5746 31.824 37.0496 31.824H36.0996V13.4556L36.681 13.7007C36.8007 13.7507 36.9261 13.7748 37.0496 13.7748C37.4194 13.7748 37.7722 13.5569 37.9255 13.1933C38.1288 12.7101 37.902 12.1534 37.4182 11.9495L19.3684 4.34961C19.1329 4.25142 18.8674 4.25142 18.6319 4.34961L0.582091 11.9495C0.0975965 12.1535 -0.128471 12.7102 0.0748116 13.1933C0.278688 13.6779 0.839182 13.9009 1.31855 13.7007ZM30.3996 31.824H7.59991V28.024H30.3996V31.824ZM30.3996 26.124H7.59991V22.3241H30.3996V26.124ZM30.3996 20.4248H7.59991V16.6248H30.3996V20.4248ZM3.79996 12.6557L18.9998 6.25538L34.1996 12.6551V31.824H32.2996V16.6241C32.2996 15.5747 31.4491 14.7242 30.3996 14.7242H7.59991C6.55047 14.7242 5.69993 15.5747 5.69993 16.6241V31.824H3.79996V12.6557Z"
                                                fill="#F6BB42" />
                                            <path d="M19 6.04546L3.88635 12.5227V13.8182L19 7.77273L34.1136 13.9942V12.5227L19 6.04546Z"
                                                fill="#E2A63B" />
                                            <path
                                                d="M35.1172 3.6535C34.7507 3.34817 34.2061 3.39767 33.9006 3.76412C33.9002 3.76466 33.8997 3.76525 33.8992 3.76579L33.2687 4.52589V1.72739C33.2687 1.25032 32.882 0.863632 32.4049 0.863632C31.9279 0.863632 31.5412 1.25032 31.5412 1.72739V4.52589L30.9106 3.76579C30.6187 3.38844 30.0763 3.31923 29.6989 3.61107C29.3216 3.90297 29.2524 4.44541 29.5442 4.82276C29.5571 4.83944 29.5706 4.85563 29.5848 4.87134L31.7442 7.46261C32.05 7.82873 32.5947 7.87764 32.9608 7.57182C33.0004 7.53878 33.037 7.50218 33.07 7.46261L35.2294 4.87134C35.5347 4.50484 35.4852 3.96024 35.1188 3.6548C35.1183 3.65442 35.1177 3.65399 35.1172 3.6535Z"
                                                fill="#FFCE54" />
                                            <path
                                                d="M35.1171 3.6535C35.0566 3.60443 34.9898 3.56378 34.9184 3.53258C35.0547 3.83316 35.0078 4.18514 34.7975 4.43952L32.6381 7.03078C32.3663 7.35129 31.9033 7.42973 31.5411 7.21649L31.7441 7.46266C32.0499 7.82878 32.5946 7.87769 32.9608 7.57187C33.0004 7.53883 33.0369 7.50223 33.07 7.46266L35.2294 4.8714C35.5347 4.5049 35.4852 3.9603 35.1187 3.65485C35.1182 3.65442 35.1176 3.65399 35.1171 3.6535Z"
                                                fill="#E2A63B" />
                                            <path
                                                d="M33.2688 4.5259C33.0303 4.5259 32.8369 4.33253 32.8369 4.09402V1.29551C32.8364 1.17329 32.8098 1.05258 32.7592 0.941376C33.0687 1.08049 33.2681 1.38799 33.2688 1.72739V3.12665V4.5259Z"
                                                fill="#E2A63B" />
                                        </g>
                                        <defs>
                                            <clipPath id="clip0_1161_1561">
                                                <rect width="38" height="38" fill="white" />
                                            </clipPath>
                                        </defs>
                                    </svg>
                                </div>
                    
                    
                    
                            </div>
                        </div>
                    </div>
                </div>
     
    </section>

  <!--- Footer --->
    <div class="my-3 nav-bg">
        <div class="max-w-4xl mx-auto bg-slate-40">
            <div>
                <h5 class="text-center text-white">@ First Couries Co.,LTd 2015-2022</h5>
            </div>
        </div>
    </div>

   
  <!--- End Footer--->

    </body>
</html>
